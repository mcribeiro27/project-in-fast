from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware

from app.adapters import routers
from app.infrastructure.sqlalchemy import Base
from app.infrastructure.sqlalchemy.database_manager import DatabaseManager

app = FastAPI(
    title="API",
    version="v1.0",
    swagger_ui_parameters={"defaultModelsExpandDepth": -1},
)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
    # allow_credentials=True,
)

db_manager = DatabaseManager()


# Configurando o banco de dados para executar consultas assíncronas
@app.on_event("startup")
async def startup_event():
    # Criando as tabelas no banco de dados
    async with db_manager.engine.begin() as conn:
        await conn.run_sync(Base.metadata.create_all)


# Esta função será usada para fechar a conexão do banco de dados ao encerrar o aplicativo
@app.on_event("shutdown")
async def shutdown_event():
    await db_manager.engine.dispose()


routers.setup(app)

if __name__ == "__main__":
    import uvicorn

    uvicorn.run("main:app", host="0.0.0.0", port=8000, reload=True)
