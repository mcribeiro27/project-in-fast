import uuid

from fastapi import HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from app.adapters.repositories.user_repositories import UserRepository
from app.adapters.schemas.users.user_in import UserIn
from app.adapters.schemas.users.user_out import UserOut
from app.infrastructure.crypto import get_password_hash


class UserService:
    def __init__(self, db: AsyncSession):
        self.user_repository = UserRepository(db)

    async def create_user(self, users: UserIn) -> UserOut:
        user = await self.get_user(users.email)
        if user:
            raise HTTPException(status_code=400, detail="O usuário já existe")
        user_dict = {
            **users.__dict__,
            "id": str(uuid.uuid4()),
            "password": get_password_hash(users.password),
        }
        return await self.user_repository.create_user(UserIn(**user_dict))

    async def get_user(self, email: str) -> UserOut:
        user = await self.user_repository.get_user(email)
        return user

    def add_role(self, users: UserIn, role_id):
        user = self.get_user(users.email)
        if not user:
            raise HTTPException(status_code=404, detail="O Usuário não existe")

        return self.user_repository.add_role(users, role_id)
