import uuid

from fastapi import HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from app.adapters.repositories.role_repositories import RoleRepository
from app.adapters.schemas.roles.roles_in import RoleIn
from app.adapters.schemas.roles.roles_out import RoleOut


class RoleService:
    def __init__(self, db: AsyncSession):
        self.role_repository = RoleRepository(db)

    async def create_role(self, roles: RoleIn) -> RoleOut:
        role = await self.get_role(roles.name)
        if role:
            raise HTTPException(status_code=400, detail="Permissão já existe.")
        role_dict = {**roles.__dict__, "id": str(uuid.uuid4())}
        return await self.role_repository.create_role(RoleIn(**role_dict))

    async def get_role(self, name: str) -> RoleOut:
        role = await self.role_repository.get_role(name)
        return role
