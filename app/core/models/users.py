from sqlalchemy import Boolean, Column, DateTime, ForeignKey, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from app.infrastructure.sqlalchemy import Base


class Users(Base):
    __tablename__ = "users"

    id = Column(String(50), primary_key=True)
    name = Column(String(250))
    email = Column(String(250), unique=True)
    password = Column(String(200))
    is_active = Column(Boolean, default=True)
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    role_id = Column(String(50), ForeignKey("roles.id"), nullable=True)
    role = relationship("Roles", back_populates="users")
