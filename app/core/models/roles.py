from sqlalchemy import Column, DateTime, String
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func

from app.infrastructure.sqlalchemy import Base


class Roles(Base):
    __tablename__ = "roles"

    id = Column(String(50), primary_key=True)
    name = Column(String(50), unique=True)
    description = Column(String(250))
    created_at = Column(DateTime(timezone=True), server_default=func.now())
    updated_at = Column(DateTime(timezone=True), onupdate=func.now())
    users = relationship("Users", back_populates="role")
