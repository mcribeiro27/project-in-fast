from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from app.adapters.schemas.roles.roles_in import RoleIn
from app.adapters.schemas.roles.roles_out import RoleOut
from app.core.models.roles import Roles


class RoleRepository:
    def __init__(self, db: AsyncSession):
        self._db = db

    async def create_role(self, roles: RoleIn) -> RoleOut:
        role = Roles(**roles.__dict__)
        self._db.add(role)
        await self._db.commit()
        await self._db.refresh(role)
        return role

    async def get_role(self, name: str) -> RoleOut:
        smtp = select(Roles).where(Roles.name == name)
        result = await self._db.execute(smtp)
        return result.scalar_one_or_none()
