from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.future import select

from app.adapters.schemas.users.user_in import UserIn
from app.adapters.schemas.users.user_out import UserOut
from app.core.models.users import Users


class UserRepository:
    def __init__(self, db: AsyncSession):
        self._db = db

    async def create_user(self, users: UserIn) -> UserOut:
        user = Users(**users.__dict__)
        self._db.add(user)
        await self._db.commit()
        await self._db.refresh(user)
        return user

    async def get_user(self, email: str) -> UserOut:
        smtp = select(Users).where(Users.email == email)
        result = await self._db.execute(smtp)
        return result.scalar_one_or_none()

    async def add_role(self, user: UserIn, role_id):
        user.role_id = role_id

        self._db.add(user)
        await self._db.commit()
        await self._db.flush()
        return user
