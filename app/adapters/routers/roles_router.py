from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from app.adapters.schemas.roles.roles_in import RoleIn
from app.adapters.schemas.roles.roles_out import RoleOutUser
from app.core.services.role_service import RoleService
from app.infrastructure.sqlalchemy import get_db

router = APIRouter()


@router.post("/roles", response_model=RoleOutUser)
async def create_role(roles: RoleIn, db: AsyncSession = Depends(get_db)):
    service = RoleService(db)
    role = await service.create_role(roles)
    return RoleOutUser(**role.__dict__)


@router.get("/roles/{name}", response_model=RoleOutUser)
async def get_role_by_name(name: str, db: AsyncSession = Depends(get_db)):
    service = RoleService(db)
    role = await service.get_role(name)
    if not role:
        raise HTTPException(status_code=404, detail="Permissão não encontrada.")
    return RoleOutUser(**role.__dict__)
