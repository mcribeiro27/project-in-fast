from fastapi.applications import FastAPI
from starlette.middleware.cors import CORSMiddleware

from app.adapters.routers import roles_router, users_router


def setup(app: FastAPI):
    app.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_methods=["*"],
        allow_headers=["*"],
        allow_credentials=True,
    )

    app.include_router(users_router.router, tags=["Usuários"])
    app.include_router(roles_router.router, tags=["Permissões"])

    return app
