from fastapi import APIRouter, Depends, HTTPException
from sqlalchemy.ext.asyncio import AsyncSession

from app.adapters.schemas.users.user_in import AddRoles, UserIn
from app.adapters.schemas.users.user_out import UserOut
from app.core.services.user_service import UserService
from app.infrastructure.sqlalchemy import get_db

router = APIRouter()


@router.post("/users", response_model=UserOut)
async def register(users: UserIn, db: AsyncSession = Depends(get_db)):
    user_service = UserService(db)
    user = await user_service.create_user(users)
    return UserOut(**user.__dict__)


@router.get("/users/{email}", response_model=UserOut)
async def get_email(email: str, db: AsyncSession = Depends(get_db)):
    user_service = UserService(db)
    user = await user_service.get_user(email)
    if not user:
        raise HTTPException(status_code=404, detail="Usuário não encontrado")
    return UserOut(**user.__dict__)


@router.patch("users/{role_id}", response_model=AddRoles)
async def add_role(user: UserIn, role_id: str, db: AsyncSession = Depends(get_db)):
    user_service = UserService(db)
    users = user_service.add_role(user, role_id)
    return users
