from datetime import datetime
from typing import Optional

from pydantic import BaseModel


class RoleId(BaseModel):
    id: Optional[str] = None


class RoleIn(RoleId):
    name: str
    description: str
    created_at: datetime
    updated_at: datetime

    class Config:
        orm_mode = True
