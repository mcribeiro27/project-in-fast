from typing import List

from pydantic import BaseModel

from app.adapters.schemas.users.user_in import UserIn


class RoleOut(BaseModel):
    id: str
    name: str
    description: str


class RoleOutUser(RoleOut):
    users: List[UserIn] = []
