import re
from datetime import datetime
from typing import Optional

from pydantic import BaseModel, validator


class UserId(BaseModel):
    id: Optional[str] = None


class UserIn(UserId):
    name: str
    email: str
    password: str
    is_active: bool
    created_at: datetime

    class Config:
        orm_mode = True

    @validator("email")
    @classmethod
    def email_validator(cls, v_email: str) -> str:
        """
        Função para validar se o formato do email está correto
        """
        regex = r"^[a-zA-Z0-9._-]+@([a-z0-9]+)(\.[a-z]{2,3})+$"
        if not re.search(regex, v_email):
            raise ValueError("Email não é valido")
        return v_email


class AddRoles(UserIn):
    role_in: str
