from pydantic import BaseModel

from app.adapters.schemas.roles.roles_out import RoleOut


class UserOut(BaseModel):
    id: str
    name: str
    email: str
    is_active: bool


class GetUser(UserOut):
    role: RoleOut
