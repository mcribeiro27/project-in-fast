import asyncio

from dynaconf import settings
from sqlalchemy.orm import sessionmaker

from sqlalchemy.ext.asyncio import (  # isort:skip
    AsyncSession,  # isort:skip
    async_scoped_session,  # isort:skip
    create_async_engine,  # isort:skip
)


class DatabaseManager:
    """
    Aqui decidimos qual o banco de dados usar
    """

    def __init__(self) -> None:
        db_url = settings.SQLALCHEMY_DATABASE_URI

        print(f"Accessing database via:::: {db_url}")
        self.__engine = create_async_engine(db_url, echo=True)  # type: ignore
        self.__async_session = sessionmaker(
            bind=self.engine, class_=AsyncSession, expire_on_commit=False  # type: ignore
        )
        self.__session = async_scoped_session(
            self.__async_session, scopefunc=asyncio.current_task  # type: ignore
        )

    @property
    def engine(self):
        """
        Getter engine
        """
        return self.__engine

    @engine.setter
    def engine(self, value):
        """
        Setter engine
        """
        self.__engine = value

    @property
    def session(self):
        """
        Getter sesion
        """
        return self.__session

    @session.setter
    def session(self, value):
        """
        Setter session
        """
        self.__session = value

    def create_session(self) -> AsyncSession:
        """
        Cria uma nova session
        """
        return self.session()

    def get_async_session(self) -> sessionmaker:
        """
        Retorna a fábrica da sessão assíncrona.
        """
        return self.__async_session
