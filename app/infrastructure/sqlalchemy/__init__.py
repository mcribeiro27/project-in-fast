from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.declarative import declarative_base

from .database_manager import DatabaseManager

Base = declarative_base()


async def get_db():
    """
    Busca um banco de dados
    """
    db_manager = DatabaseManager()
    dbase: AsyncSession = db_manager.create_session()  # type: ignore
    try:
        yield dbase
    finally:
        await dbase.close()
